//
//  QYTableViewCell.h
//  Quickly
//
//  Created by Maksim Vialykh on 14.09.15.
//  Copyright © 2015 Maksim Vialykh. All rights reserved.
//

#import <UIKit/UIKit.h>

@class QYRestaurantItem;

@interface QYTableViewCell : UITableViewCell

@property (nonatomic, weak) QYRestaurantItem *data;

@end
