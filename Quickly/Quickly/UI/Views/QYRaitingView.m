//
//  QYRaitingView.m
//  Quickly
//
//  Created by Maksim Vialykh on 14.09.15.
//  Copyright © 2015 Maksim Vialykh. All rights reserved.
//

#import "QYRaitingView.h"

#define kSpaceBetweenStars 2.0f

@implementation QYRaitingView


#pragma mark - Initialization

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        [self prepareForUse];
    }
    
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self prepareForUse];
}

- (void)prepareForUse {
    self.currentRaRaiting = 0;
    self.maxRaiting = 5;
}


#pragma mark - Property

- (void)setMaxRaiting:(NSInteger)maxRaiting {
    _maxRaiting = maxRaiting;
    [self setNeedsDisplay];
}

- (void)setCurrentRaRaiting:(NSInteger)currentRaRaiting {
    _currentRaRaiting = currentRaRaiting;
    [self setNeedsDisplay];
}


#pragma mark -

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    CGFloat freeSpace = kSpaceBetweenStars * (_maxRaiting - 1);
    CGFloat oneStarWidth = (CGRectGetWidth(self.frame) - freeSpace) / _maxRaiting;
    CGFloat originX = 0.0f;
    CGFloat originY = (CGRectGetHeight(self.frame) - oneStarWidth) / 2;
    NSInteger startIndex = 0;
    
    while (startIndex < _currentRaRaiting) {
        originX = startIndex * freeSpace + startIndex * oneStarWidth;
        
        UIImage *starImage = [UIImage imageNamed:@"unnamed"];
        [starImage drawInRect:CGRectMake(originX, originY, oneStarWidth, oneStarWidth)];
        startIndex++;
    }
    
}

@end
