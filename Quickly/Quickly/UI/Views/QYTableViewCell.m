//
//  QYTableViewCell.m
//  Quickly
//
//  Created by Maksim Vialykh on 14.09.15.
//  Copyright © 2015 Maksim Vialykh. All rights reserved.
//

#import "QYTableViewCell.h"
#import "QYRaitingView.h"

#import "QYRestaurantItem.h"

#import "QYConfiguration.h"

#import "UIImageView+WebCache.h"

@implementation QYTableViewCell {
    __weak IBOutlet UIView *_containerView;
    __weak IBOutlet UILabel *_titleLabel;
    __weak IBOutlet UIImageView *_iconImageView;
    __weak IBOutlet QYRaitingView *_raitingView;
    __weak IBOutlet UILabel *_cousinesLabel;
    __weak IBOutlet UILabel *_addressLabel;
    __weak IBOutlet UIView *_typeView;
    __weak IBOutlet UILabel *_typeLabel;
    __weak IBOutlet UILabel *_locationLabel;
    __weak IBOutlet UILabel *_reviewLabel;
    __weak IBOutlet UILabel *_minPriceLabel;
    __weak IBOutlet UILabel *_transportabel;
    __weak IBOutlet UILabel *_reliverTimeLabel;
    __weak IBOutlet UILabel *_otherInfoLabel;
}


#pragma mark - Initialization

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _containerView.layer.cornerRadius = 10.0f;
    _containerView.backgroundColor = [UIColor whiteColor];
    _typeView.layer.cornerRadius = 5.0f;
}


#pragma mark -

- (void)setData:(QYRestaurantItem *)data {
    if ([_data isEqual:data] == NO) {
        _data = data;
        
        _titleLabel.text = _data.name;
        NSURL *url = [NSURL URLWithString:[kImages_ENDPOINT stringByAppendingString:_data.picture]];
        [_iconImageView sd_setImageWithURL:url];
        _cousinesLabel.text = _data.type_cousines;
        _addressLabel.text = _data.address;
        _minPriceLabel.text = _data.min_price;
        _transportabel.text = _data.delivery_time;
        _raitingView.currentRaRaiting = rand() % 5;
    }
}

@end
