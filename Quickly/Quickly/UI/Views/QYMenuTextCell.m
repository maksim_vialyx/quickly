//
//  QYMenuTextCell.m
//  Quickly
//
//  Created by Maksim Vialykh on 15.09.15.
//  Copyright © 2015 Maksim Vialykh. All rights reserved.
//

#import "QYMenuTextCell.h"

@implementation QYMenuTextCell {
    __weak IBOutlet UIImageView *_cellImageView;
    __weak IBOutlet UILabel *_cellTitle;
    
}


#pragma mark -

- (void)prepareForReuse {
    [super prepareForReuse];
    
    [self setImage:[UIImage imageNamed:@"unnamed"] title:LS(@"Кухни")];
}


#pragma mark -

- (void)setImage:(UIImage *)image title:(NSString *)title {
    _cellImageView.image = image;
    _cellTitle.text = title;
}

@end
