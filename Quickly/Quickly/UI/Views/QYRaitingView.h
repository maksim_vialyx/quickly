//
//  QYRaitingView.h
//  Quickly
//
//  Created by Maksim Vialykh on 14.09.15.
//  Copyright © 2015 Maksim Vialykh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QYRaitingView : UIView

@property (nonatomic) NSInteger maxRaiting;
@property (nonatomic) NSInteger currentRaRaiting;

@end
