//
//  QYMenuImageCell.m
//  Quickly
//
//  Created by Maksim Vialykh on 15.09.15.
//  Copyright © 2015 Maksim Vialykh. All rights reserved.
//

#import "QYMenuImageCell.h"

@implementation QYMenuImageCell {
    __weak IBOutlet UIImageView *_cellImageView;
    __weak IBOutlet UILabel *_cellTitleLabel;
    __weak IBOutlet UILabel *_cellDescriptionLabel;
}


#pragma mark - 

- (void)prepareForReuse {
    [super prepareForReuse];
    
    [self setData:nil];
}


#pragma mark - 

- (void)setData:(id)data {
    _cellImageView.image = [UIImage imageNamed:@"unnamed"];
    _cellTitleLabel.text = LS(@"Иван");
    _cellDescriptionLabel.text = LS(@"Изменить профиль");
}

@end
