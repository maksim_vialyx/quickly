//
//  QYMenuTextCell.h
//  Quickly
//
//  Created by Maksim Vialykh on 15.09.15.
//  Copyright © 2015 Maksim Vialykh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QYMenuTextCell : UITableViewCell

- (void)setImage:(UIImage *)image title:(NSString *)title;

@end
