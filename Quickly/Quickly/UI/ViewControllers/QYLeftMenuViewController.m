//
//  QYLeftMenuViewController.m
//  Quickly
//
//  Created by Maksim Vialykh on 15.09.15.
//  Copyright © 2015 Maksim Vialykh. All rights reserved.
//

#import "QYLeftMenuViewController.h"

#import "QYMenuImageCell.h"
#import "QYMenuTextCell.h"

@implementation QYLeftMenuViewController


#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIButton *bottomLink = [UIButton buttonWithType:UIButtonTypeCustom];
    [bottomLink setTitle:LS(@"Quickly.com") forState:UIControlStateNormal];
    [bottomLink addTarget:self action:@selector(pressBottonLink) forControlEvents:UIControlEventTouchUpInside];
    bottomLink.frame = CGRectMake(0, CGRectGetHeight(self.view.frame) - 30.0f, CGRectGetWidth(self.view.frame), 30.0f);
    
    self.tableView.tableFooterView = bottomLink;
}


#pragma mark - Actions

- (void)pressBottonLink {
    NSLog(@"press bottom link");
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger rowsNumber = 0;
    if (section == 0) {
        rowsNumber = 1;
    } else if (section == 1) {
        rowsNumber = 7;
    }
    return rowsNumber;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:indexPath.section == 0 ? NSStringFromClass([QYMenuImageCell class]) : NSStringFromClass([QYMenuTextCell class])];
    
    if (indexPath.section == 0) {
        QYMenuImageCell *_cell = cell;
        [_cell setData:nil];
        cell = _cell;
    } else if (indexPath.section == 1) {
        QYMenuTextCell *_cell = cell;
        [_cell setImage:[UIImage imageNamed:@"unnamed"] title:LS(@"Название пункта")];
        cell = _cell;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = 0.0f;
    if (indexPath.section == 0) {
        height = 130.0f;
    } else if (indexPath.section == 1) {
        height = 50.0f;
    }
    return height;
}


#pragma mark - UITableViewDelegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"selected indexPath: %@", indexPath);
    
    return nil;
}

@end
