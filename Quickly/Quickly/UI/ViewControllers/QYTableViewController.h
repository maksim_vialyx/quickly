//
//  QYTableViewController.h
//  Quickly
//
//  Created by Maksim Vialykh on 14.09.15.
//  Copyright © 2015 Maksim Vialykh. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SlideNavigationController.h"

@interface QYTableViewController : UITableViewController <SlideNavigationControllerDelegate>

@end
