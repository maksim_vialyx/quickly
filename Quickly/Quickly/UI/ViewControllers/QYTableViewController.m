//
//  QYTableViewController.m
//  Quickly
//
//  Created by Maksim Vialykh on 14.09.15.
//  Copyright © 2015 Maksim Vialykh. All rights reserved.
//

#import "QYTableViewController.h"

#import "QYTableViewCell.h"

#import "QYRestaurantItem.h"
#import "RLMResults.h"

#import "QYAPIClient.h"

NSString *const kCellIdentifier = @"restaurantCellIdentifier";

@implementation QYTableViewController {
    BOOL _isLoading;
    RLMResults *_items;
}


#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self applyUI];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([QYTableViewCell class]) bundle:[NSBundle mainBundle]] forCellReuseIdentifier:kCellIdentifier];
    
    [self updateData];
}

- (void)applyUI {
    self.title = [LS(@"Рестораны") uppercaseString];
    self.view.backgroundColor = RGB(0.0f, 0.0f, 0.0f);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
//    UIButton *menuImage = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 35, 35)];
//    [menuImage setImage:[UIImage imageNamed:@"unnamed"] forState:UIControlStateNormal];
//    [menuImage addTarget:self action:@selector(navigationBarLeft) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:menuImage];
//    self.navigationItem.leftBarButtonItem = leftButton;
    
    UIButton *shopButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 35, 35)];
    [shopButton setImage:[UIImage imageNamed:@"unnamed"] forState:UIControlStateNormal];
    [shopButton addTarget:self action:@selector(navigationBarLeft) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:shopButton];
    self.navigationItem.rightBarButtonItem = rightButton;
}


#pragma mark - Actions

- (void)navigationBarLeft {
    
}

- (void)navigationBarRight {
    
}


#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 154.5f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    QYTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    if (!cell) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([QYTableViewCell class]) owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    [cell setData:[_items objectAtIndex:indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.backgroundColor = [UIColor clearColor];
}


#pragma mark - 

- (void)updateData {
    [self.refreshControl beginRefreshing];
    [[QYAPIClient instance] loadRestaurantsList:^(RLMResults *items, NSError *error) {
        _items = items;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
            [self.refreshControl endRefreshing];
        });
    }];
}


#pragma mark - SlideNavigationControllerDelegate

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu {
    return YES;
}

@end
