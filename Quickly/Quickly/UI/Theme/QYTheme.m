//
//  QYTheme.m
//  Quickly
//
//  Created by Maksim Vialykh on 14.09.15.
//  Copyright © 2015 Maksim Vialykh. All rights reserved.
//

#import "QYTheme.h"

#import <UIKit/UIKit.h>

@implementation QYTheme


#pragma mark - Public

- (void)applyDefaultTheme {
    [self applyNavigationBarAppearance];
}


#pragma mark - Work

- (void)applyNavigationBarAppearance {
    [[UINavigationBar appearance] setTranslucent:NO];
    [[UINavigationBar appearance] setBarTintColor:RGB(38.0f, 43.0f, 49.0f)];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
}

@end
