//
//  QYRestaurantItem.h
//  Quickly
//
//  Created by Maksim Vialykh on 14.09.15.
//  Copyright © 2015 Maksim Vialykh. All rights reserved.
//

#import "RLMObject.h"

@interface QYRestaurantItem : RLMObject

@property NSString *ID;
@property NSString *name;
@property NSString *picture;
@property NSString *type_cousines;
@property NSString *min_price;
@property NSString *delivery_price;
@property NSString *delivery_time;
@property NSString *special_deal;
@property NSString *interior_picture;
@property NSString *reception_time;
@property NSString *address;
@property NSString *working_time;
@property NSString *important_info;
@property NSString *panorama_3d;
@property NSString *cousine_id;
@property NSString *city;

+ (QYRestaurantItem *)itemFromJSON:(id)JSON;

@end
