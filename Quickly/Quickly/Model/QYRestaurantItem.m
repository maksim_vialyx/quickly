//
//  QYRestaurantItem.m
//  Quickly
//
//  Created by Maksim Vialykh on 14.09.15.
//  Copyright © 2015 Maksim Vialykh. All rights reserved.
//

#import "QYRestaurantItem.h"

@implementation QYRestaurantItem


#pragma mark -

+ (NSDictionary *)defaultPropertyValues {
    return @{@"ID": @"",
             @"name": @"",
             @"picture": @"",
             @"type_cousines": @"",
             @"min_price": @"",
             @"delivery_price": @"",
             @"delivery_time": @"",
             @"special_deal": @"",
             @"interior_picture": @"",
             @"reception_time": @"",
             @"address": @"",
             @"working_time": @"",
             @"important_info": @"",
             @"panorama_3d": @"",
             @"cousine_id": @"",
             @"city": @""};
}


#pragma mark - 

+ (QYRestaurantItem *)itemFromJSON:(id)JSON {
    QYRestaurantItem *item = [QYRestaurantItem new];
    
    item.ID = _string(JSON[@"ID"]);
    item.name = JSON[@"name"];
    item.picture = JSON[@"picture"];
    item.type_cousines = JSON[@"type_cousines"];
    item.min_price = JSON[@"min_price"];
    item.delivery_price = JSON[@"delivery_price"];
    item.special_deal = JSON[@"special_deal"];
    item.interior_picture = JSON[@"interior_picture"];
    item.reception_time = JSON[@"reception_time"];
    item.address = JSON[@"address"];
    item.working_time = JSON[@"working_time"];
    item.important_info = JSON[@"important_info"];
    item.panorama_3d = JSON[@"panorama_3d"];
    item.cousine_id = JSON[@"cousine_id"];
    item.city = JSON[@"city"];
    
    return item;
}

@end
