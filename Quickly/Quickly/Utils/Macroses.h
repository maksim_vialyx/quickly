//
//
//

#define LS(v) NSLocalizedString(v, nil)


// Color

#define RGBA(r, g, b, a) [UIColor colorWithRed:(float)(r) / 255.f green:(float)(g) / 255.f blue:(float)(b) / 255.f alpha:a]
#define RGB(r, g, b) RGBA(r, g, b, 1)


//NSNumber + String

#define _integer(v) ((v == nil) ? 0 : [v integerValue])
#define _string(v)  ((v == nil) ? @"" : ([v isKindOfClass:[NSNull class]]) ? @"" : ([v isKindOfClass:[NSString class]]) ? v : ([v isKindOfClass:[NSNumber class]]) ? [NSString stringWithFormat:@"%@", v] : @"")
#define _float(v)   ((v == nil) ? 0 : [v floatValue])
#define _double(v)  ((v == nil) ? 0 : [v doubleValue])


// AppDelegate

#define _AppDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])


// Detect iphone

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)


// Other

#define RADIANS(degrees) ((degrees * M_PI) / 180.0)
