//
//  QYAPIClient.m
//  Quickly
//
//  Created by Maksim Vialykh on 14.09.15.
//  Copyright © 2015 Maksim Vialykh. All rights reserved.
//

#import "QYAPIClient.h"

#import <Realm.h>
#import "QYRestaurantItem.h"

#import <AFNetworking.h>

#import "QYConfiguration.h"


@implementation QYAPIClient {
    AFHTTPRequestOperationManager *_requestManager;
}


#pragma mark - Initialization

+ (instancetype)instance {
    static QYAPIClient* _instance = nil;
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        _instance = [[self class] new];
    });
    
    return _instance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _requestManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:kAPI_ENDPOINT]];
        _requestManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    }
    return self;
}


#pragma mark - 

- (void)loadRestaurantsList:(ItemsCompletion)completion {
    [_requestManager POST:kAPI_ENDPOINT parameters:@{@"method": @"getAllRestaurants"}
                 success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
                     RLMRealm *realm = [RLMRealm defaultRealm];
                     [realm beginWriteTransaction];
#warning HARD RESET
                     [realm deleteAllObjects];
        
                     for (NSDictionary *data in responseObject) {
                         QYRestaurantItem *item = [QYRestaurantItem itemFromJSON:data];
                         [realm addObject:item];
                     }
        
                     [realm commitWriteTransaction];
        
                     if (completion) {
                         completion([QYRestaurantItem allObjects], nil);
                     }
    } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
        NSLog(@"loadRestaurantsList error: %@", error);
        if (completion) {
            completion(nil, error);
        }
    }];
}

@end
