//
//  QYAPIClient.h
//  Quickly
//
//  Created by Maksim Vialykh on 14.09.15.
//  Copyright © 2015 Maksim Vialykh. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RLMResults;

typedef void (^ItemsCompletion)(RLMResults *items, NSError *error);

@interface QYAPIClient : NSObject

+ (instancetype)instance;

- (void)loadRestaurantsList:(ItemsCompletion)completion;

@end
